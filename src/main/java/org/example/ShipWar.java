import java.util.Scanner;

public class ShipWar {
    public static void arrangeShips(String player, int[][] dask, Scanner scanner) {
        printDask(player, dask);

        for (int i = 1; i <= 4; i++) {

            System.out.println("Введите " + i + "-координату");

            int x = scanner.nextInt();
            int y = scanner.nextInt();

            if ((x > dask.length || x < 1) && (y > dask.length || y < 1)) {
                System.out.println("Вы ввели неправильные координаты!");
                i--;
            } else if (dask[x - 1][y - 1] == 1) {
                System.out.println("Эта ячейка уже занята");
                i--;
            } else {
                dask[x - 1][y - 1] = 1;
            }
        }

        printDask(player, dask);
    }

    public static void printDask(String player, int[][] dask) {
        System.out.println(player + ", вот ваша доска!");

        for (int i = 0; i < dask.length; i++) {
            System.out.print(i + 1  + ".  ");

            for (int j = 0; j < dask[i].length; j++) {
                System.out.print(dask[i][j] + " ");
            }
            System.out.println();
        }

        for (int i = 0; i < 2; i++) {
            System.out.println();
        }

    }

    public static void attackDask(String player, int[][] dask, Scanner scanner) {
        System.out.println(player + ", введите координаты огня!");

        int x = scanner.nextInt();
        int y = scanner.nextInt();

        if (dask[x - 1][y - 1] == 1) {
            System.out.println("Вы попали по кораблю противника!");
            dask[x - 1][y - 1] = 0;
        } else {
            System.out.println("Вы не попали по кораблю!");
        }
    }

    public static boolean checkDask(String player, int[][] dask) {
        for (int i = 0; i < dask.length; i++) {
            for (int j = 0; j < dask[i].length; j++) {
                if (dask[i][j] == 1) return false;
            }
        }

        System.out.println(player + " проиграл игру! Игра окончена!");
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String player1 = scanner.nextLine();
        String player2 = scanner.nextLine();

        int daskSize = 5;

        int[][] dask1 = new int[daskSize][daskSize];
        int[][] dask2 = new int[daskSize][daskSize];

        arrangeShips(player1, dask1, scanner);
        arrangeShips(player2, dask2, scanner);

        while (true) {

            attackDask(player1, dask2, scanner);

            if (checkDask(player2, dask2)) {
                break;
            }

            attackDask(player2, dask1, scanner);

            if (checkDask(player1, dask1)) {
                break;
            }

        }
    }

}

